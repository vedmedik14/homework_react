

export function visible() {
    document.querySelector(".form-wrapper").classList.add("visible");
    document.querySelector(".creat-note-text").innerHTML='';
}

export function editVisiable(text) {
    document.querySelector(".edit-form-wrapper").classList.add("visible");
    document.querySelector(".edit-note-text").innerHTML=text;
}

export function hidden() {
    document.querySelector(".form-wrapper").classList.remove("visible");
    document.querySelector(".edit-form-wrapper").classList.remove("visible");
}

//
    export function CreatForm({saveFC}) {
        return(
            <div className="form-wrapper">
                <div contentEditable="true" className="creat-note-text"></div>
                <button className="save-img" onClick={()=>{saveFC(document.querySelector(".creat-note-text").innerHTML)}}>&#128191;</button>
            </div>
        )
    }

    export function EditForm({editBt}) {
        return(
            <div className="edit-form-wrapper">
                <div contentEditable="true" className="edit-note-text"></div>
                <button className="edit-img" onClick={()=>{editBt(document.querySelector(".edit-note-text").innerHTML)}}>EDIT</button>
            </div>
        )
    }
//