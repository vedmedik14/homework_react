import { Component } from "react";
import { AddBt } from "../addBt";

import { visible, hidden, editVisiable} from "../../metods";
import {CreatForm, EditForm} from "../../metods";

import "./app.css" 



export class App extends Component{
    state={
        notesArr : [], 
    }

    removeEvent=(key)=>{
        this.setState((state)=>{
            return{
                notesArr : state.notesArr.filter(el => el.key !== key),
            }
        });
    }

    readyEvent=(key)=>{
        document.querySelector(`#${key} > .note-text`).classList.add("crossed");
    }
//
    editEvent=(noteText)=>{
        editVisiable(noteText);
        this.setState((state)=>{
            return{
                notesArr : state.notesArr.filter(el => el.key !== noteText),
            }
        });
    }

    editBt=(noteText)=>{

        hidden();

        this.setState((state)=>{
            return {
                notesArr : this.state.notesArr.concat([this.CreatNote(noteText)]).reverse(),
            }
        });
    }

//
    CreatNote(noteText){
        return(
            <div className="note-wrapper" id={noteText} key={noteText}>
                <div className="note-text">{noteText}</div>
                <div className="bt-container">
                    <button className="bt-note edit-note" onClick={()=>{this.editEvent(noteText)}}>✍️</button>
                    <button className="bt-note ready-note" onClick={()=>{this.readyEvent(noteText)}}>✅</button>
                    <button className="bt-note delit-note" onClick={()=>{this.removeEvent(noteText)}}>❌</button>
                </div>
            </div>
        )
    }

    saveFC=(noteText)=>{

        hidden();

        if (noteText !== "" && noteText !== undefined){
            this.setState((state)=>{
                return {
                    notesArr : this.state.notesArr.concat([this.CreatNote(noteText)]).reverse(),
                }
            });
        }else{
            alert('write your text')
        }
    }

    render(){
        
        return(
            <>
                <AddBt f={visible}></AddBt>
                <div className="notes-wrapper">
                    {this.state.notesArr.map((el)=>{
                        return(
                            el
                        )
                    })}
                </div>
                <EditForm editBt={this.editBt}></EditForm>
                <CreatForm saveFC={this.saveFC}></CreatForm>
            </>
        )
    }

}