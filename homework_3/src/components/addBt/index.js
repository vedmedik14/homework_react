
import "./addBt.css"

export function AddBt({f}) {
    return (
        <div className="add-bt-wrapper"> 
            <button className="add-bt" onClick={()=>{f()}}>&#10133;</button>
        </div>
    )
}