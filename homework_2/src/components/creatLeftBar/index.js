import React from "react";

import {LeftProfile} from "./leftProfile";
import {Search} from "./search";
import {CreatPosition} from "./creatPosition";
import { TemaSwitch } from "./temaSwitch";

import search from "../../assets/icons/search.svg"
import home from "../../assets/icons/home.svg"
import analytics from "../../assets/icons/analytics.svg"
import inventory from "../../assets/icons/inventory.svg"
import notification from "../../assets/icons/notification.svg"
import revenue from "../../assets/icons/revenue.svg"
import logout from "../../assets/icons/logout.svg"

import "./leftBar.css"

export function CreatLeftBar() {
    return(
        <div className="leftBar bar">
            <LeftProfile/>
            <div className="positions">
                <Search placeholder={"Search..."}/>                
                <CreatPosition icon={home} className={"anim"} text={"Dashboard"}></CreatPosition>
                <CreatPosition icon={notification} className={"anim"} text={"Notifications"}></CreatPosition>
                <CreatPosition icon={revenue} className={"anim"} text={"Revenue"}></CreatPosition>
                <CreatPosition icon={analytics} className={"anim"} text={"Analytics"}></CreatPosition>
                <CreatPosition icon={inventory} className={"anim"} text={"Inventory"}></CreatPosition>
            </div>
            <div className="bottomMenu">
                <CreatPosition icon={logout} className={"anim"} text={"Logout"}></CreatPosition>
                <TemaSwitch></TemaSwitch>                
            </div>
        </div>
    )
}