import React from "react";

import "./switchInput.css"

export function SwitchInput({className}) {
    return (
        <label className={className+" switchWrapper"}>
            <input type="checkbox" className="switchInput"></input>
            <span className="switchSlider"></span>
        </label>
    )
}