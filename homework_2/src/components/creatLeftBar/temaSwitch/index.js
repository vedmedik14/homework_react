import React from "react";

import { SwitchInput } from "./switchInput";

import sun from "../../../assets/icons/sun.svg"

import "./switch.css"

export function TemaSwitch({className}) {
    return (
        <div className={className + " punkt"}>
            <div className="imgWrapper">
                <img src={sun} alt="alternativ"></img>
            </div>
            <div className="textWrapper">
                <h3 className="text">Light mode</h3>
            </div>
            <SwitchInput></SwitchInput>
        </div>
    )
}