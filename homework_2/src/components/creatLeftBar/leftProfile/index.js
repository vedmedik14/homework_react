import React from "react";

import { Avatar} from "./avatar"

import "./profile.css"

import profileImg from "../../../assets/imgs/profile.svg"

export function LeftProfile() {
    return(
        <div className="leftProfileContainer">
            <Avatar></Avatar>
            <div className="infoWrapper">
                <h3>AnimatedFred</h3>
                <h5>animated@demo.com</h5>
            </div>
        </div>
    )
}