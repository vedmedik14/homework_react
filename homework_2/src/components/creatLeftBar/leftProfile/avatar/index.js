import React from "react";

import "./avatar.css"

import profileImg from "../../../../assets/imgs/profile.svg"

export function Avatar({className}) {
    return(
        <div className={className+" profileImgWrapper"}>
            <img src={profileImg} alt="alternative"></img>
        </div>
    )
}