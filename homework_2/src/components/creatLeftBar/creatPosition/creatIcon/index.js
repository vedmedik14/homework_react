import React from "react";

import "./icon.css"

export function CreatIcon({icon, className}) {
    return(
        <div className={className +" imgWrapper"}>
            <img src={icon} alt="alternativ"></img>
        </div>
    )
}