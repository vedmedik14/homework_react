import React from "react";

import { CreatIcon } from "./creatIcon";

import "./position.css"

export function CreatPosition({icon, className, text}) {
    return(
        <div className={className + " punkt"}>
            <CreatIcon icon={icon}></CreatIcon>
            <div className="textWrapper">
                <h3 className="text">{text}</h3>
            </div>
        </div>
    )
}