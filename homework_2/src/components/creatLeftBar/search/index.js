import React from "react";

import "./search.css"

export function Search ({placeholder, className, inputClass}) {
    return(
        <div className={className + " screachWrapper"}>
            <input className={inputClass +" search"} placeholder={placeholder}></input>
        </div>
    )
}