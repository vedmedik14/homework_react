import React from "react";

import {CreatLeftBar} from "../creatLeftBar";
import {CreatRightBar} from "../creatRightBar";

import "./main.css"

export function Main() {
    return (
        <>
            <div className="left_area area"></div>       
            <CreatLeftBar/>     
            <div className="right_area area"></div>         
            <CreatRightBar/>
        </>
    )
}