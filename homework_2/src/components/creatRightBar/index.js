import React from "react";

import { CreatIcon } from "../creatLeftBar/creatPosition/creatIcon";
import {Avatar} from "../creatLeftBar/leftProfile/avatar"
import { Search } from "../creatLeftBar/search";
import { SwitchInput } from "../creatLeftBar/temaSwitch/switchInput";

import search from "../../assets/icons/search.svg"
import home from "../../assets/icons/home.svg"
import revenue from "../../assets/icons/revenue.svg"
import notification from "../../assets/icons/notification.svg"
import analytics from "../../assets/icons/analytics.svg"
import inventory from "../../assets/icons/inventory.svg"
import logout from "../../assets/icons/logout.svg"

import "./rightBar.css"


export function CreatRightBar() {
    return(
        <div className="rightBar bar">
            <Avatar className={"r_profileImgWrapper"}></Avatar>
            <div className="positions r_position">
                <Search className={"r_screachWrapper"} inputClass={"r_search"}></Search>
                <CreatIcon className={"r_icon"} icon={home}></CreatIcon>
                <CreatIcon className={"r_icon"} icon={revenue}></CreatIcon>
                <CreatIcon className={"r_icon"} icon={notification}></CreatIcon>
                <CreatIcon className={"r_icon"} icon={analytics}></CreatIcon>
                <CreatIcon className={"r_icon"} icon={inventory}></CreatIcon>
            </div>
            <div className="bottomMenu r_bottomMenu">
                <CreatIcon className={"r_icon"} icon={logout}></CreatIcon>
                <SwitchInput className={"r_switchWrapper"}></SwitchInput>
            </div>
        </div>
    )
}