import React from "react";
import ReactDOM from "react-dom";

function ZodiacList() {
    return (
        <table>
            <thead>
                <tr>
                    <th>Назва</th>
                    <th>Дата</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Козеріг</td>
                    <td>1.01-20.01</td>
                </tr>
                <tr>
                    <td>Водолій</td>
                    <td>21.01-18.02</td>
                </tr>
                <tr>
                    <td>Риби</td>
                    <td>19.02-20.03</td>
                </tr>
                <tr>
                    <td>Овен</td>
                    <td>21.03-20.04</td>
                </tr>
                <tr>
                    <td>Телець</td>
                    <td>21.04-21.05</td>
                </tr>
                <tr>
                    <td>Близнюки</td>
                    <td>22.05-21.06</td>
                </tr>
                <tr>
                    <td>Рак</td>
                    <td>22.06-22.07</td>
                </tr>
                <tr>
                    <td>Лев</td>
                    <td>23.07-23.08</td>
                </tr>
                <tr>
                    <td>Діва</td>
                    <td>24.08-23.09</td>
                </tr>
                <tr>
                    <td>Терези</td>
                    <td>24.09-23.10</td>
                </tr>
                <tr>
                    <td>Скорпіон</td>
                    <td>24.10-22.11</td>
                </tr>
                <tr>
                    <td>Стрілець</td>
                    <td>23.11-21.12</td>
                </tr>
            </tbody>
        </table>
    )
}
ReactDOM.render(<ZodiacList></ZodiacList>, document.querySelector(".one"));


const App =()=> <h1>hello world</h1>

function name() {
    <h1>hello world</h1>
}